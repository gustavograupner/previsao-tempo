<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/previsao-tempo.css">
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Insert title here</title>
	</head>
<body>
	<jsp:useBean id="dao" class="br.com.previsaotempo.dao.PrevisaoTempoDAO"/>
	
	<h2>Previs�o do Tempo - Cidades Cadastradas</h2>
	
	<table>
	  <c:forEach var="cidade" items="${dao.cidades}">
	    <tr>
	      <td>${cidade.nome}</td>
	      <td><a href="exibidetalhe?cidade=${cidade.nome}">Ver Previs�o</a></td>
	    </tr>
	  </c:forEach>
	</table>
	
	<a class="link-navegacao" href="index.jsp">Voltar para a home</a>
</body>
</html>