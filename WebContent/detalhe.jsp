<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/previsao-tempo.css">
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h2>Previs�o do Tempo - Detalhe Previs�o do Tempo para ${param.cidade}</h2>
	<h4>Pr�ximos 5 dias a cada 3 horas</h4>
	
	<table>
		<tr>
	    	<th>Data Previs�o</th>
	    	<th>Condi��o</th>
	    	<th>Temp. Min.</th> 
	    	<th>Temp. Max.</th>
	    	<th>Umidade</th>
  		</tr>
	    <c:forEach var="previsaoTempo" items="${cidadeDetalhe.listaPrevisaoTempo}">			
			<tr>
				<td>${previsaoTempo.data}</td>
			 	<td>${previsaoTempo.descricaoPrevisao}</td>
				<td>${previsaoTempo.temperaturaMinima}</td>
				<td>${previsaoTempo.temperaturaMaxima}</td>
				<td>${previsaoTempo.umidade}</td>
			</tr>
		</c:forEach>
	</table>
	
	<a class="link-navegacao" href="listagem.jsp">Voltar para a lista</a>
</body>
</html>