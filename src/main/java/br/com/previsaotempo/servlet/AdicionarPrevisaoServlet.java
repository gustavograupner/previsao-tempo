package br.com.previsaotempo.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.previsaotempo.service.PrevisaoTempoService;

@WebServlet("/adicionaPrevisao")
public class AdicionarPrevisaoServlet extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Object cidade = request.getParameter("cidade");
		if (cidade != null) {
			PrevisaoTempoService previsaoTempoService = new PrevisaoTempoService();
			
			RequestDispatcher rd = request.getRequestDispatcher(previsaoTempoService.carregaPrevisao(cidade.toString()));
			rd.forward(request,response);
		}
	}
}
