package br.com.previsaotempo.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.previsaotempo.model.Cidade;
import br.com.previsaotempo.service.PrevisaoTempoService;

@WebServlet("/exibidetalhe")
public class DetalhePrevisaoServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Object cidade = request.getParameter("cidade");
		if (cidade != null) {
			PrevisaoTempoService previsaoTempoService = new PrevisaoTempoService();
			Cidade detalhe = previsaoTempoService.buscaDetalhePrevisao(cidade.toString());			
			if (detalhe != null) {
				request.setAttribute("cidadeDetalhe", detalhe);
			}
			RequestDispatcher rd = request.getRequestDispatcher("/detalhe.jsp");
			rd.forward(request,response);
		}
	}
}
