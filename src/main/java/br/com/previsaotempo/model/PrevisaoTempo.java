package br.com.previsaotempo.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class PrevisaoTempo implements Serializable {	
	
    private static final long serialVersionUID = -6993373071284531852L;
    
	private LocalDateTime data;	
	private String descricaoPrevisao; 
	private Double temperaturaMinima;
	private Double temperaturaMaxima;
	private Integer umidade;
	
	public LocalDateTime getData() {
		return data;
	}
	public void setData(LocalDateTime data) {
		this.data = data;
	}
	public String getDescricaoPrevisao() {
		return descricaoPrevisao;
	}
	public void setDescricaoPrevisao(String descricaoPrevisao) {
		this.descricaoPrevisao = descricaoPrevisao;
	}
	public Double getTemperaturaMinima() {
		return temperaturaMinima;
	}
	public void setTemperaturaMinima(Double temperaturaMinima) {
		this.temperaturaMinima = temperaturaMinima;
	}
	public Double getTemperaturaMaxima() {
		return temperaturaMaxima;
	}
	public void setTemperaturaMaxima(Double temperaturaMaxima) {
		this.temperaturaMaxima = temperaturaMaxima;
	}
	public Integer getUmidade() {
		return umidade;
	}
	public void setUmidade(Integer umidade) {
		this.umidade = umidade;
	}	
	
	@Override
	public String toString() {
		return "Previsao [descricao : " + descricaoPrevisao 
				+ ", data : " + data.toString() 
				+ ", temp Maxima : " + temperaturaMaxima 
				+ ", temp Minima : " + temperaturaMinima
				+ ", umidade : " + umidade
				+ "]";
	}	
}
