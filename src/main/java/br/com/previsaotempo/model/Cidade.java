package br.com.previsaotempo.model;

import java.io.Serializable;
import java.util.List;

public class Cidade implements Serializable {	
	
    private static final long serialVersionUID = -2852562048598772770L;
    
	private String nome;
	private List<PrevisaoTempo> listaPrevisaoTempo;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public List<PrevisaoTempo> getListaPrevisaoTempo() {
		return listaPrevisaoTempo;
	}
	public void setListaPrevisaoTempo(List<PrevisaoTempo> listaPrevisaoTempo) {
		this.listaPrevisaoTempo = listaPrevisaoTempo;
	}
	
	@Override
	public String toString() {
		return "Cidade [nome : " + nome + ", previs�es : " + listaPrevisaoTempo.toString() + "]";
	}	
}
