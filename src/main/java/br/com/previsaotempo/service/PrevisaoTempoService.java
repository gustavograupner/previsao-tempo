package br.com.previsaotempo.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import br.com.previsaotempo.dao.PrevisaoTempoDAO;
import br.com.previsaotempo.exception.BuscadorPrevisaoDoTempoException;
import br.com.previsaotempo.model.Cidade;
import br.com.previsaotempo.model.PrevisaoTempo;
import br.com.previsaotempo.util.BuscadorPrevisaoDoTempo;
import br.com.previsaotempo.util.ConversorDeTemperatura;

public class PrevisaoTempoService {
	
	public String carregaPrevisao(String nomeCidade) {
		try {
			JSONObject retornoApi = BuscadorPrevisaoDoTempo.busca(nomeCidade);		
			
			Integer codigoRetorno = Integer.valueOf(retornoApi.get("cod").toString());
			if (codigoRetorno == 200) {
				List<PrevisaoTempo> previsoesDoTempo = converteJsonToBean(retornoApi);
				
				Cidade cidade = new Cidade();
				cidade.setNome(nomeCidade);
				cidade.setListaPrevisaoTempo(previsoesDoTempo);
				
				new PrevisaoTempoDAO().salva(cidade);
				return "/sucesso.jsp";
			} else {
				return "/error.jsp";
			}			
		} catch (BuscadorPrevisaoDoTempoException bpdte) {
			return "/error.jsp";
		}
	}
	
	public Cidade buscaDetalhePrevisao(String nomeCidade) {
		PrevisaoTempoDAO previsaoTempoDAO = new PrevisaoTempoDAO();
		return previsaoTempoDAO.getCidade(nomeCidade);
	}
	
	public List<PrevisaoTempo> converteJsonToBean(JSONObject json) {
		JSONArray list = (JSONArray) json.get("list");
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		List<PrevisaoTempo> previsoesTempo = new ArrayList<>();
		
		for (Object listChild : list) {
			PrevisaoTempo previsaoTempo = new PrevisaoTempo();
			
			JSONObject previsoesJson = ((JSONObject) listChild);			
			
			JSONObject main = (JSONObject) previsoesJson.get("main");
			
			Double tempMinima = ConversorDeTemperatura.kelvinParaCelsius(Double.valueOf(main.get("temp_min").toString()));
			Double tempMaxima = ConversorDeTemperatura.kelvinParaCelsius(Double.valueOf(main.get("temp_max").toString()));
			
			previsaoTempo.setTemperaturaMinima(tempMinima);
			previsaoTempo.setTemperaturaMaxima(tempMaxima);
			previsaoTempo.setUmidade(Integer.valueOf(main.get("humidity").toString()));
			
			previsaoTempo.setData(LocalDateTime.parse(previsoesJson.get("dt_txt").toString(), formatter));
			
			JSONArray weather = (JSONArray) previsoesJson.get("weather");
			for (Object weatherChild : weather) {
				previsaoTempo.setDescricaoPrevisao(((JSONObject) weatherChild).get("description").toString());
			}		
			
			previsoesTempo.add(previsaoTempo);
		}		
		
		return previsoesTempo;
	}
}
