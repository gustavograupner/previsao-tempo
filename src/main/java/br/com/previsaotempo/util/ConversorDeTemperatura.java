package br.com.previsaotempo.util;

public class ConversorDeTemperatura {
	
	public static Double kelvinParaCelsius(Double kelvin) {
		return kelvin - 273;
	}
	
	public static Double celsiusParaKelvin(Double celsius) {
		return celsius + 273;
	}	
}
