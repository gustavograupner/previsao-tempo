package br.com.previsaotempo.util;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class LeitorJson {
	
	public static JSONObject parse(InputStream inputStream) {
		JSONParser parser = new JSONParser();
		try {			 
			Reader reader = new InputStreamReader(inputStream, "UTF-8");
			return (JSONObject) parser.parse(reader); 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
