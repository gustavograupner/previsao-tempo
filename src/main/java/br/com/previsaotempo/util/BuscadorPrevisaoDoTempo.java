package br.com.previsaotempo.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.simple.JSONObject;

import br.com.previsaotempo.exception.BuscadorPrevisaoDoTempoException;

public class BuscadorPrevisaoDoTempo  {
	
	private static final String URL_API_JSON = "http://api.openweathermap.org/data/2.5/forecast?q=%s&APPID=eb8b1a9405e659b2ffc78f0a520b1a46";
	
	public static JSONObject busca(String nomeCidade) throws BuscadorPrevisaoDoTempoException {
		HttpURLConnection connection = null;        
		try {
			URL url = new URL(String.format(URL_API_JSON, nomeCidade.toLowerCase()));	    
			connection = (HttpURLConnection) url.openConnection();			
			try {
				return LeitorJson.parse(connection.getInputStream());
			} catch (FileNotFoundException fne) {
				throw new BuscadorPrevisaoDoTempoException("N�o foi poss�vel buscar a cidade informada, tente novamente.");
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			connection.disconnect();
		}
	}
}