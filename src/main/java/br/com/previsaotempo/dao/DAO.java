package br.com.previsaotempo.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class DAO {
	
	protected void salva(String nomeArquivo, Object objeto) {
		if (nomeArquivo != null && !nomeArquivo.isEmpty()) {
			File arquivo = new File(nomeArquivo);
			try {
				FileOutputStream fos;  
		        fos = new FileOutputStream(arquivo); 
		        ObjectOutputStream oos = new ObjectOutputStream(fos); 
		        oos.writeObject(objeto);
			    oos.close();
	        } catch (IOException e) {
		        e.printStackTrace();
	        }
		}
	}
	
	protected Object busca(String nomeArquivo) {
		Object retorno = null;
		try {
			FileInputStream fis = new FileInputStream(new File(nomeArquivo));
	        ObjectInputStream ois = new ObjectInputStream(fis);   
	        try {
	            retorno = ois.readObject();
            } catch (ClassNotFoundException e) {
	            e.printStackTrace();
            } 
	        ois.close();
		} catch (IOException e) {
	        e.printStackTrace();
        }
		return retorno;
	}
}
