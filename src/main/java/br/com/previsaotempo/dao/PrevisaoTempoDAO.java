package br.com.previsaotempo.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.previsaotempo.model.Cidade;

public class PrevisaoTempoDAO extends DAO {
	
	private final String FILE_NAME = "CIDADE.dat";
	
	public void salvaLista(List<Cidade> cidade) {
		super.salva(FILE_NAME, cidade);
	}
	
	public void salva(Cidade cidade) {
		List<Cidade> cidades = new ArrayList<>();		
		List<Cidade> valoresAnteriores = getCidades();
		if (valoresAnteriores != null) {
			cidades.addAll(valoresAnteriores);
		}		
		cidades.add(cidade);	
		
		this.salvaLista(cidades);
	}
	
	public Cidade getCidade(String nomeCidade) {
		if (nomeCidade != null) {
			List<Cidade> cidades = getCidades();
			if (cidades != null) {
				for (Cidade cidade : cidades) {
					if (cidade.getNome().equals(nomeCidade)) {
						return cidade;
					}
				}
			}
		}
		return null;
	}
	
	public List<Cidade> getCidades() {
	    Object cidades = super.busca(FILE_NAME);
	    if (cidades != null) {
	    	return (List<Cidade>) cidades;
	    }
	    return null;
	}	
}
