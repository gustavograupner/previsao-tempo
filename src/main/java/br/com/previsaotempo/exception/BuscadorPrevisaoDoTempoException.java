package br.com.previsaotempo.exception;

public class BuscadorPrevisaoDoTempoException extends Exception {
	
	public BuscadorPrevisaoDoTempoException(String msg) {
		super(msg);
	}
}
