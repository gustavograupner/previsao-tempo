- Projeto desenvolvido para realizar a busca de informações sobre a previsão do tempo
de determinada cidade, através da API openweather. A API retorna um JSON com a previsão
dos próximos 5 dias, com intervalo de 3 horas.

O JSON retornado é processado, e deste, são extraídas as seguintes informações:
	- Data e hora da leitura;
	- Temperatura mínima;
	- Temperatura máxima;
	- Umidade do ar;
	- Descrição previsão;

Para representação destas informações, são utilizadas as classes Cidade.java e PrevisaoTempo.java. Que por 
sua vez, são armazenadas em um arquivo ".dat", através da classe PrevisaoTempoDAO. 

O projeto foi implementado utilizando a arquitetura MVC. Para rodar o projeto, basta importar no eclipse, 
e adicionar no servidor Tomcat, também será necessário utilizar Java 8 para compilação.